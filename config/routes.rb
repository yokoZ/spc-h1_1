Rails.application.routes.draw do


  root 'static_pages#home'

	get    '/login',  to: 'session#new'
	post   '/login',  to: 'session#create'
	delete '/logout', to: 'session#destroy'

  resources :comments
  resources :users
  resources :issues
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
