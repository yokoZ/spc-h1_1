class SessionController < ApplicationController
  def new
  end

  def create
  	# ユーザ取得
    @user = User.find_by(email: params[:session][:email].downcase)

    # ユーザが存在　かつ　パスワードが適切かチェック
    if @user && @user.password == params[:session][:password]

      # ユーザIDをセッションに保存
      log_in @user
      redirect_to issues_url
    end
  end

  def destroy
    # ログインしている場合のみログアウト
    log_out if logged_in?
    redirect_to root_url

  end
end
