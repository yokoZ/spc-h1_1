class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  # 全ての機能から利用する為にセッション機能をインクルード
  include SessionHelper

end
