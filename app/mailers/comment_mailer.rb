class CommentMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.comment_mailer.posted_information.subject
  #
  def posted_information(issue, comment)
    users = User.all
    @issue = issue
    @comment = comment

    to_address = ''
    users.each{|u|
      if to_address
        to_address = u.email
      else
        to_address += ',' + u.email
      end
    }
    mail to: to_address
  end
end
