class User < ApplicationRecord
  # DB保存前にメールアドレスを小文字に変換する
  before_save :downcase_email

  # avaterカラムと画像の関連付け（carrierwave)
  mount_uploader :avater, PictureUploader
  
	# nameを必須、50文字までとする
	validates :name, presence: true, length: { maximum: 50 }

	# emailを必須、255文字までとする、形式が正しい事、ユニークであること
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
										uniqueness: { case_sensitive: false }

  # パスワードが必須で6文字以上であること
  validates :password, presence: true, length: { minimum: 6 }

  private 

    # メールアドレスをすべて小文字にする
    def downcase_email
       self.email = email.downcase
    end

end
