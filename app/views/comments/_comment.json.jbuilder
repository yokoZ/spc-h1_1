json.extract! comment, :id, :issue_no, :comment, :post_user, :created_at, :updated_at
json.url comment_url(comment, format: :json)
