json.extract! issue, :id, :issue_name, :content, :owner, :start, :end, :prog_rate, :status, :created_at, :updated_at
json.url issue_url(issue, format: :json)
