class CreateIssues < ActiveRecord::Migration[5.1]
  def change
    create_table :issues do |t|
      t.string :issue_name
      t.text :content
      t.integer :owner
      t.date :start
      t.date :end
      t.integer :prog_rate
      t.integer :status

      t.timestamps
    end
  end
end
