class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.integer :issue_no
      t.text :comment
      t.integer :post_user

      t.timestamps
    end
  end
end
