require 'test_helper'

class CommentMailerTest < ActionMailer::TestCase
  test "posted_information" do
    mail = CommentMailer.posted_information
    assert_equal "Posted information", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
